################ Makefile supports both C and C++ #############################

#Define list of include diretories
IDIR= ./src ./opencv

INC_PARAMS=$(foreach d, $(IDIR), -I$d)

#Define list of directories for source files
SOURCES = $(wildcard opencv/*.cpp) \
	  $(wildcard src/*.c) 
	  

#Define list of macro definitions
DEFINES = M_PI=3.14159265358979323846
DEF_PARAMS=$(foreach d, $(DEFINES), -D$d)
SPEEX_DEFINES = FLOATING_POINT __EMX__ DISABLE_WARNINGS  HAVE_CONFIG_H USE_SMALLFT DISABLE_WARNINGS
SPEEX_DEF_PARAMS=$(foreach d, $(SPEEX_DEFINES), -D$d)

#Define linked libraries
LIBS = ft4222 m asound wiringPi
LIB_PARAMS=$(foreach d, $(LIBS), -l$d)
LIB_PATHS = -Wl,-rpath ./target_rootfs/usr/lib/arm-linux-gnueabihf/ -L./target_rootfs/usr/lib -L./target_rootfs/usr/local/lib

#Define compile options
WARNS = -Wall -Wextra

CFLAGS= -Ofast -lstdc++ -mfloat-abi='hard' ${WARNS} ${DEF_PARAMS} ${INC_PARAMS} ${SPEEX_DEF_PARAMS} -pthread ${LIB_PARAMS} ${LIB_PATHS} #-include config.h
CFLAGS += --sysroot=/home/huyk/Projects/OpenCV/Hub/target_rootfs

CPPFLAGS = -D__STDC_LIMIT_MACROS ${INC_PARAMS} ${LIB_PATHS} \
	   -lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_ml -lopencv_video \
	   -lopencv_calib3d -lopencv_objdetect -lopencv_imgcodecs -lopencv_features2d \
	   -lopencv_dnn -lopencv_flann -lopencv_videoio \
	   -ldlib \
	   -I./target_rootfs/usr/include/opencv -I./target_rootfs/usr/include/opencv2 -I./target_rootfs/usr/include -I./target_rootfs/usr

#CC=gcc
CC=arm-linux-gnueabihf-gcc
CXX=arm-linux-gnueabihf-g++

TARGET_EXEC ?= hub
BUILD_DIR ?= ./build
OBJS := $(SOURCES:%=$(BUILD_DIR)/%.o)
DEPS := $(OBJS:.o=.d)

$(BUILD_DIR)/$(TARGET_EXEC): $(OBJS)
	$(CXX) $(CFLAGS) $(CPPFLAGS) $(OBJS) -o $@ $(LDFLAGS) -lft4222 -lasound -lm -lwiringPi

# assembly
$(BUILD_DIR)/%.s.o: %.s
	$(MKDIR_P) $(dir $@)
	$(AS) $(ASFLAGS) -c $< -o $@

# c source
$(BUILD_DIR)/%.c.o: %.c
	$(MKDIR_P) $(dir $@)
	$(CC) $(CFLAGS) $(CFLAGS) -c $< -o $@

# c++ source
$(BUILD_DIR)/%.cpp.o: %.cpp
	$(MKDIR_P) $(dir $@)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@

.PHONY: clean

clean:
	$(RM) -r $(BUILD_DIR)

-include $(DEPS)

MKDIR_P ?= mkdir -p

