//#include "opencv2/objdetect.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
//#include "opencv2/videoio.hpp"
#include "opencv2/core/types.hpp"
#include <iostream>
#include "opencv_face_detect.h"

using namespace std;
using namespace cv;

#define USE_CASCADE (1)
#define USE_DNN_SSD (0)
#define USE_DLIB    (0)
#define SKIP_FRAMES 4

#if USE_CASCADE
#define CASCADE_XML     "data/haarcascade_frontalface_default.xml"  //180ms
//#define CASCADE_XML     "data/haarcascade_frontalface_alt2.xml"     //240ms
//#define CASCADE_XML     "data/haarcascade_frontalface_alt.xml"        //240ms
//#define CASCADE_XML     "data/haarcascade_frontalface_alt_tree.xml"     //no detection
//#define CASCADE_XML     "data/haarcascade_profileface.xml"      //no detection

CascadeClassifier face_cascade;
std::vector<Rect> Faces;
#endif

#if USE_DNN_SSD
#include <opencv2/dnn.hpp>
#include <opencv2/dnn/shape_utils.hpp>
using namespace cv::dnn;
dnn::Net Dnn_Net;
float Dnn_Thresh = 0.5;

static Mat fd_Preprocess(const Mat& frame);
#endif

#if USE_DLIB
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing.h>
#include <dlib/image_io.h>
#include <dlib/opencv.h>

using namespace dlib;
frontal_face_detector Dlib_Detector;
shape_predictor Dlib_ShapePredict;
#endif


int CV_FaceDetect_Init(void)
{

#if USE_CASCADE
    if( !face_cascade.load(CASCADE_XML) )
    {
        cout << "--(!)Error loading face cascade\n";
        return -1;
    };
#elif USE_DNN_SSD
    //! [Create the importer of Caffe model]
    Ptr<dnn::Importer> importer;
    try
    {
        importer = dnn::createCaffeImporter("data/deploy.prototxt.txt", 
                                            "data/res10_300x300_ssd_iter_140000.caffemodel");
        
    }
    catch (const cv::Exception &err) //Importer can throw errors, we will catch them
    {
        cerr << err.msg << endl;
    }
    //! [Create the importer of Caffe model]

    if (!importer)
    {
        cerr << "Can't load network by using the following files: " << endl;
        cerr << "Models can be downloaded here:" << endl;
        cerr << "https://github.com/weiliu89/caffe/tree/ssd#models" << endl;
        exit(-1);
    }

    //! [Initialize network]
    importer->populateNet(Dnn_Net);
    importer.release();          //We don't need importer anymore
#elif USE_DLIB
    Dlib_Detector = get_frontal_face_detector();
    deserialize("data/shape_predictor_68_face_landmarks.dat") >> Dlib_ShapePredict;
#endif


    printf("Done\n");
    return 0;
}

//To detect faces in a grayscale image
std::vector<Rect> CV_FaceDetect_Process(Mat frame)
{
    static int count = 0;
    count++;
    if(count % SKIP_FRAMES != 0)
    {
        return Faces;
    }
#if USE_CASCADE
    //equalizeHist( frame, frame );
    face_cascade.detectMultiScale( frame, Faces );
    
#elif USE_DNN_SSD
    Mat preprocessedFrame = fd_Preprocess(frame);
    Mat inputBlob = blobFromImage(preprocessedFrame);   //Convert Mat to batch of images
    Dnn_Net.setInput(inputBlob, "data");                //set the network input
    Mat detection = Dnn_Net.forward("detection_out");   //compute output
    Mat detectionMat(detection.size[2], detection.size[3], CV_32F, detection.ptr<float>());
    float confidenceThreshold = Dnn_Thresh;
#if 1
    for(int i = 0; i < detectionMat.rows; i++)
    {
        float confidence = detectionMat.at<float>(i, 2);

        if(confidence > confidenceThreshold)
        {
            size_t objectClass = (size_t)(detectionMat.at<float>(i, 1));

            float xLeftBottom = detectionMat.at<float>(i, 3) * frame.cols;
            float yLeftBottom = detectionMat.at<float>(i, 4) * frame.rows;
            float xRightTop = detectionMat.at<float>(i, 5) * frame.cols;
            float yRightTop = detectionMat.at<float>(i, 6) * frame.rows;
            Rect object((int)xLeftBottom, (int)yLeftBottom,
                        (int)(xRightTop - xLeftBottom),
                        (int)(yRightTop - yLeftBottom));

            //rectangle(frame, object, Scalar(0, 255, 0));
            Faces.push_back(object);
        }
    }
#endif
#elif USE_DLIB
    //faces = Dlib_Detector(frame);
    //To convert OpenCV Mat into dlib Mat for RGB frame
    printf("1\n");
    Mat im_small;
    cv::resize(frame, im_small, cv::Size(), 1.0/4, 1.0/4);
    printf("2\n");
    cv_image<bgr_pixel> cimg_small(im_small);
    cv_image<bgr_pixel> cimg(frame);
    //dlib::array2d<dlib::bgr_pixel> dlibImage;
    //dlib::assign_image(dlibImage, dlib::cv_image<dlib::bgr_pixel>(im_small));
    //for gray frame
    //dlib::array2d<unsigned char> dlibImageGray;
    //dlib::assign_image(dlibImageGray, dlib::cv_image<unsigned char>(cvMatImageGray));
    printf("3\n");
    //std::vector<dlib::rectangle> dets = Dlib_Detector(dlibImage);
    std::vector<dlib::rectangle> dets = Dlib_Detector(cimg_small);
    printf("4\n");
    for (unsigned long i = 0; i < dets.size(); ++i)
    {
        printf("5\n");
    }
    for(dlib::rectangle i: dets) {
        printf("ok\n");
        //Convert dlib rectangle to opencv rectangle
        Faces.push_back(cv::Rect(cv::Point2i(i.left(), i.top()), cv::Point2i(i.right() + 1, i.bottom() + 1)));
        //To convert opencv rectangle to dlib rectangle
        //dlib::rectangle((long)r.tl().x, (long)r.tl().y, (long)r.br().x - 1, (long)r.br().y - 1);
    }
#endif
    return Faces;
}

#if USE_DNN_SSD
const size_t width = 300;
const size_t height = 300;
static Mat fd_GetMean(const size_t& imageHeight, const size_t& imageWidth)
{
    Mat mean;

    const int meanValues[3] = {104, 117, 123};
    vector<Mat> meanChannels;
    for(int i = 0; i < 3; i++)
    {
        Mat channel((int)imageHeight, (int)imageWidth, CV_32F, Scalar(meanValues[i]));
        meanChannels.push_back(channel);
    }
    cv::merge(meanChannels, mean);
    return mean;
}

static Mat fd_Preprocess(const Mat& frame)
{
    Mat preprocessed;
    frame.convertTo(preprocessed, CV_32F);
    resize(preprocessed, preprocessed, Size(width, height)); //SSD accepts 300x300 RGB-images
    Mat mean = fd_GetMean(width, height);
    cv::subtract(preprocessed, mean, preprocessed);
    return preprocessed;
}

#endif