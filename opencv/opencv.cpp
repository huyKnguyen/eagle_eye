#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include <sstream>
#include "opencv_face_detect.h"
#include "opencv.h"

using namespace std;
using namespace cv;

extern "C" int CV_Init( void )
{
    //TODO: Any initialization for OpenCV
    CV_FaceDetect_Init();
    return 0;
}

extern "C" void CV_Process(unsigned char *buf, int len)
{
    double t = (double)getTickCount();

    Mat img;
    //To read JPEG image from a raw buffer
    Mat rawData( 1, len, CV_8UC1, (void*)buf );
    img = imdecode(rawData, CV_LOAD_IMAGE_GRAYSCALE); //for original color, use CV_LOAD_IMAGE_UNCHANGED
    //img = imdecode(rawData, CV_LOAD_IMAGE_UNCHANGED);
    if(countNonZero(img) < 1)   //only working with Grayscale image
    {
        cout << "Invalid jpg" << endl;
        return;
    }
#if 1
    //Do face detection
    std::vector<Rect> faces = CV_FaceDetect_Process(img);
    if(faces.size() == 0)
    {
        cout << "No face detected" << endl;
    }
    else 
    {
        for ( size_t i = 0; i < faces.size(); i++ )
        {
            Point center( faces[i].x + faces[i].width/2, faces[i].y + faces[i].height/2 );
            ellipse( img, center, Size( faces[i].width/2, faces[i].height/2 ), 0, 0, 360, Scalar( 255, 0, 255 ), 4 );
        }
    }
#endif
    //Display processing time
    t = ((double)getTickCount() - t)/getTickFrequency();
    cout << "Times passed in seconds: " << t << endl;

    imwrite("/home/huy/out/image.jpeg", img);

}