#ifndef OPENCV_FACE_DETECT_H
#define OPENCV_FACE_DETECT_H

#include "opencv2/core.hpp"
#include "opencv2/objdetect.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include <iostream>
#include <vector>

using namespace std;
using namespace cv;
#ifdef __cplusplus
extern "C" {
#endif


int CV_FaceDetect_Init(void);
std::vector<Rect> CV_FaceDetect_Process(Mat frame);

#ifdef __cplusplus
}
#endif
#endif