#ifndef OPENCV_H
#define OPENCV_H

#ifdef __cplusplus
extern "C" {
#endif

int CV_Init( void );
void CV_Process(unsigned char *buf, int len);

#ifdef __cplusplus
}
#endif
#endif