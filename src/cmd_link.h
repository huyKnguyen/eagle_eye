#ifndef CMD_LINK_H
#define CMD_LINK_H

#include <stdint.h>
#include <stdbool.h>

#define MAX_REQUEST_DATA    200
#define MAX_EVT_DATA        30    //max size of data sent along with notificaton from device

#define CMD_ACK     0xA5
#define CMD_NAK     0x5A
#define NOTIFY_ACK  0xB5


#define SET_REQ     0x80
#define GET_REQ     0x00
#define ACT_REQ     0x00

/* List of commands between Hub and device */
#define CMD_INVALID              0
#define CMD_SET_WIFI             (SET_REQ | 1)
#define CMD_GET_WIFI             (GET_REQ | 2)
#define CMD_SET_ROUTER           (SET_REQ | 3)
#define CMD_GET_ROUTER           (GET_REQ | 4)
#define CMD_SET_COLOR            (SET_REQ | 5)
#define CMD_GET_COLOR            (GET_REQ | 6)
#define CMD_SET_LINK             (ACT_REQ | 7)
#define CMD_SET_DEV_NAME         (SET_REQ | 8)
#define CMD_GET_DEV_INFO         (GET_REQ | 9)
#define CMD_START_VOICE_STREAM   (ACT_REQ | 10)
#define CMD_STOP_VOICE_STREAM    (ACT_REQ | 11)
#define CMD_START_CAM_STREAM     (ACT_REQ | 12)
#define CMD_STOP_CAM_STREAM      (ACT_REQ | 13)
#define CMD_START_RX_VOICE       (ACT_REQ | 14)
#define CMD_STOP_RX_VOICE        (ACT_REQ | 15)
#define CMD_START_FW_OTA         (ACT_REQ | 16)   //not tested 
#define CMD_STOP_FW_OTA          (ACT_REQ | 17)   //not tested
#define CMD_GET_SNAPSHOT         (GET_REQ | 18)
#define CMD_PING                 (ACT_REQ | 19)
#define CMD_SELF_TEST            (ACT_REQ | 20)   //not tested
#define CMD_FACTORY_RESET        (ACT_REQ | 21)
#define CMD_GET_HW_LOG           (ACT_REQ | 22)   //not tested
#define CMD_RESET                (ACT_REQ | 23)
#define CMD_SET_MOTION_DETECT    (SET_REQ | 24)
#define CMD_REPORT_CONTACT       (SET_REQ | 25)
#define CMD_GET_MOTION_DETECT    (GET_REQ | 26)
#define CMD_GET_REPORT_CONTACT   (GET_REQ | 27)
#define CMD_DATA_FW_OTA          (SET_REQ | 28)   //not tested
#define CMD_SET_UNLINK           (ACT_REQ | 29)
#define CMD_SET_AUTO_RECORD      (SET_REQ | 30)
#define CMD_GET_AUTO_RECORD      (GET_REQ | 31)
#define CMD_SET_HUB_IP           (SET_REQ | 32)   //used in Router mode
#define CMD_GET_CAM_FRAME        (ACT_REQ | 33)



/* List of notification events between Hub and device */
#define CMD_GREETING_EVENT      (1)
#define CMD_CHECK_ALIVE_EVENT   (2)
#define CMD_DISCONNECT_EVENT    (3)

#define INVALID_TCP_PORT      0xFFFFFFFF

typedef enum
{
  REQUEST_FAILED=0,     //Failed to send request or failed to receive response
  REQUEST_ACK,          //Successful send and receive ACK
  REQUEST_NAK,          //Successful send and receive NAK
}request_status_t;

typedef (*CMD_CALLBACK)(request_status_t status, uint8_t *data, uint32_t len);




void CmdLink_Init();
void CmdLink_FreeCmdSocket();
void CmdLink_ResetCmdSocket();
int CmdLink_CheckConnectStatus();
request_status_t CmdLink_SendCmd(uint8_t cmd);
bool CmdLink_SendRequest(uint8_t cmd, uint8_t *data, uint32_t len, CMD_CALLBACK callback);
bool CmdLink_SendPing();
bool CmdLink_SendGreetingToDev(char *dev_ip, uint32_t assigned_socket);
bool CmdLink_SendNotification(char *ip, uint8_t *data, uint8_t len);



#endif  //CMD_LINK_H
