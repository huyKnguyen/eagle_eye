#ifndef MAIN_H
#define MAIN_H

#define ENABLE_CAMERA     (1)
#define ENABLE_VOICE      (1)

#define VOICE_PIPE_IP         "127.0.0.1"
#define DOORBELL_IP           "192.168.4.1"   //"192.168.8.31"
#define ADHOC_IP_PATTERN      "192.168.4.xxx" //pattern of IP address when Hub connects to device's network
#define HUB_IP                "192.168.2.6"

//NOTE: If I set 100ms here, somehow it cannot record voice to send to device. It works when I set 250ms
#define GUI_RENDER_INTERVAL_MS    250

#define CMD_TCP_PORT1          6000     //TCP socket to talk with Device 1
#define CMD_TCP_PORT2          6001     //TCP socket to talk with Device 2
#define CMD_TCP_PORT3          6002     //TCP socket to talk with Device 3
#define CMD_TCP_PORT4          6003     //TCP socket to talk with Device 4
#define CMD_TCP_PORT5          6005     //TCP socket to talk with Device 5
#define INVALID_CMD_TCP_PORT   0xFFFFFFFF
#define UDP_GREETING_PORT      88888    //Hub will send greeting to this port to check if linked device is online
#define CMD_TCP_PORT           5005
#define FRAME_UDP_PORT         44444    //UDP port to receive camera frames from Doorbell
#define RX_VOICE_UDP_PORT      55555    //UDP port to receive voice from Doorbell
#define TX_VOICE_UDP_PORT      66655    //UDP port to send voice to Doorbell
#define RX_NOTIFY_UDP_PORT     77777
#define VOICE_PIPE_PORT        8888

void Printf(const char *fmt, ...);
void App_LockEveMutex();
void App_UnlockEveMutex();
void App_SetRenderInterval(int ms);


#endif	//MAIN_H