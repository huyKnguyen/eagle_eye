#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <net/if.h>
#include <net/if_arp.h>
#include <arpa/inet.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <unistd.h>
#include <wiringPi.h>
#include "cmd_link.h"
#include "main.h"
#include "gpio.h"
#include "opencv.h"
#include "wiringPi.h"

#define MAXBUF          (30*1024)
#define FRAME_OVER_TCP  (0)
#define FRAME_OVER_UDP  (1)

pthread_t Cam_Thread_Handle;
pthread_t OpenCV_Thread_Handle;
pthread_mutex_t Eve_Mutex;

int RemoteFrameSocket;
int FrameUdpSocket;                   //UDP socket Handle to receive camera frames
int RxVoiceUdpSocket;                 //UDP socket Handle to receive codec voice to Doorbell
struct sockaddr_in FrameRemoteUdp;    //UDP Bind to Remote end to receive frames

socklen_t Client_Addr_Size, Server_Addr_Size;
bool Is_Gui_Running, Is_Camera_Running;
bool Is_OpenCV_Running, Is_OpenCV_Stop = 0;
int App_RenderIntervalMs;
typedef struct {
  unsigned char *p_frame;
  unsigned char is_valid;
  unsigned char is_process;
  int len;
}frame_buf_t;
#define MAX_FRAMEBUF    3
unsigned char FrameBuf1[MAXBUF];
unsigned char FrameBuf2[MAXBUF];
unsigned char FrameBuf3[MAXBUF];
frame_buf_t FrameBuf[MAX_FRAMEBUF] = {
  {.p_frame = FrameBuf1, .is_valid = 0, .is_process = 0, .len = 0},
  {.p_frame = FrameBuf2, .is_valid = 0, .is_process = 0, .len = 0},
  {.p_frame = FrameBuf3, .is_valid = 0, .is_process = 0, .len = 0},
};
unsigned char Active_Frame_Id = 0;
int FrameLen[] = {0,0};



#define DEBUG   (1)
void Printf(const char *fmt, ...)
{
#if DEBUG
  va_list args;
  va_start(args, fmt);
  vprintf(fmt, args);
  va_end(args);
  fflush(stdout);
#endif
}


void* App_OpenCV(void *arg)
{
  int i;
  while(1)
  {
    Is_OpenCV_Running = false;
    //Wait for start
    pthread_mutex_lock(&Eve_Mutex);  
    Is_OpenCV_Running = true;
    do
    {
      digitalWrite(GPIOA_11,HIGH);
      FrameBuf[Active_Frame_Id].is_process = 1;
      CV_Process(FrameBuf[Active_Frame_Id].p_frame, FrameBuf[Active_Frame_Id].len);
      FrameBuf[Active_Frame_Id].is_process = 0;
      FrameBuf[Active_Frame_Id].is_valid = 0;
      for(i=0;i<MAX_FRAMEBUF;i++)
      {
        if(i != Active_Frame_Id && FrameBuf[i].is_valid == 1) break;
      }
      if(i == MAX_FRAMEBUF) break;
      Active_Frame_Id = i;
      digitalWrite(GPIOA_11,LOW);
    } while (Is_OpenCV_Stop == 0);
  }
}

void* App_CameraStream(void *arg)
{
  int len, total_len;
  unsigned char buf[MAXBUF];
  unsigned char rx_buf_id = 0;
  pthread_mutex_lock(&Eve_Mutex);
  do
  {
    total_len = 0;
    //Receive all UDP packets belong to 1 camare frame
    digitalWrite(GPIOA_12,HIGH);
    if(Is_OpenCV_Running == false)
    {
      rx_buf_id = 0;
    }
    else
    {
      for(int i=0;i<MAX_FRAMEBUF;i++)
      {
        if(i != rx_buf_id && FrameBuf[i].is_process == 0) {
          rx_buf_id = i;
          FrameBuf[i].is_valid = 0;
          break;
        }
      }
    }
    do 
    {
      len = recv(FrameUdpSocket, FrameBuf[rx_buf_id].p_frame + total_len , MAXBUF, 0); //receive data from client
      total_len += len;
    } while(len == 1460);
    FrameBuf[rx_buf_id].len = total_len;
    FrameBuf[rx_buf_id].is_valid = 1;
    digitalWrite(GPIOA_12,LOW);
#if 1
    if(Is_OpenCV_Running ==  false)
    {
      pthread_mutex_unlock(&Eve_Mutex);
      Is_OpenCV_Running = true;
    }
#else
    {
      FILE *fd = fopen("/home/huy/out/image.jpeg", "wb");
      fwrite(FrameBuf[rx_buf_id].p_frame, 1, total_len, fd);
      fclose(fd);
    }
#endif
  }while(1);
}

void App_LockEveMutex()
{
  pthread_mutex_lock(&Eve_Mutex);
}

void App_UnlockEveMutex()
{
  pthread_mutex_unlock(&Eve_Mutex);
}



void App_StartCamStream()
{
  //Create thread to render App GUI
  int err = pthread_create(&Cam_Thread_Handle, NULL, &App_CameraStream, NULL);
  if(err != 0)
  {
    Printf("Cannot create camera thread [%s]\n", strerror(err));
  }
  //Create a thread for OpenCV
  err = pthread_create(&OpenCV_Thread_Handle, NULL, &App_OpenCV, NULL);
  if(err != 0)
  {
    Printf("Cannot create opencv thread [%s]\n", strerror(err));
  }
}




void App_NetworkInit()
{
  struct sockaddr_storage server_storage;
  /*
    Create/Configure UDP socket to receive frames
  */
  FrameUdpSocket = socket(PF_INET, SOCK_DGRAM, 0); // create a UDP socket
  if(FrameUdpSocket<=0) 
  { 
    Printf("socket error !\n"); 
    return 0; 
  }
  struct sockaddr_in udp_port;
  udp_port.sin_family = AF_INET;
  udp_port.sin_port = htons(FRAME_UDP_PORT); 
  udp_port.sin_addr.s_addr = inet_addr("0.0.0.0");
  memset(udp_port.sin_zero, '\0', sizeof udp_port.sin_zero);  
  bind(FrameUdpSocket, (struct sockaddr *) &udp_port, sizeof(udp_port)); // bind the socket
  Client_Addr_Size = sizeof server_storage;
  
  FrameRemoteUdp.sin_family = AF_INET;
  FrameRemoteUdp.sin_port = htons(FRAME_UDP_PORT);
  FrameRemoteUdp.sin_addr.s_addr = inet_addr(DOORBELL_IP);
  memset(FrameRemoteUdp.sin_zero, '\0', sizeof FrameRemoteUdp.sin_zero);  
  Server_Addr_Size = sizeof FrameRemoteUdp; 
}

int main(void)
{ 
  unsigned char data;

  CV_Init();
  pthread_mutex_init(&Eve_Mutex, NULL);

  //setup GPIO to toogle for debugging
  wiringPiSetup();  
  pinMode(GPIOA_12, OUTPUT);
  pinMode(GPIOA_11, OUTPUT);
  digitalWrite(GPIOA_12,LOW);
  digitalWrite(GPIOA_11,LOW);
  
  Printf("Init Network\n");
  App_NetworkInit();
  CmdLink_Init(); 
  App_StartCamStream();

  

  char cmd_buf[10];
  int Exit_App = 0;
  request_status_t ret;
  char Cmd_Data[255];
  while(!Exit_App)
  {
    Printf("\nInput command:");
    int err = read(0, cmd_buf, 10);
    switch(cmd_buf[0])
    {
      case 'q':
        Exit_App = 1;
        break;
      case '1':
        ret = CmdLink_SendCmd(CMD_START_CAM_STREAM);
        Printf("\nResp=0x%x",ret);
        break;
#if 0
      case '1':   //start frame
        data=3;   
        //write(RemoteCmdSocket, &data, 1);
        ret = CmdLink_SendCmd(CMD_START_CAM_STREAM); 
        Printf("\nResp=%d",ret);
        break;
      case '2':   //stop frame
        data=4;   
        //write(RemoteCmdSocket, &data, 1);
        ret = CmdLink_SendCmd(CMD_STOP_CAM_STREAM); 
        Printf("\nResp=%d",ret);
        break;  
      case '3':   //start voice
        data=1;   
        //write(RemoteCmdSocket, &data, 1);
        ret = CmdLink_SendCmd(CMD_START_VOICE_STREAM);
        Printf("\nResp=%d",ret);
        break;
      case '4':   //stop voice
        data=2;   
        //write(RemoteCmdSocket, &data, 1);
        ret = CmdLink_SendCmd(CMD_STOP_VOICE_STREAM); 
        Sound_StopPlayback();
        Printf("\nResp=%d",ret);
        break;
      case '5':
        strcpy(Cmd_Data, "new_AP");
        len = strlen("new_AP") + 1;
        strcpy(Cmd_Data+len , "12345678");
        len += strlen("12345678") + 1;
        if(!CmdLink_SendRequest(CMD_SET_WIFI, Cmd_Data, len, NULL))  Printf("\nFailed to send request");
        Printf("\nResp=%d", Cmd_Data[0]);
        break;
      case '6':
        memset(Cmd_Data, 0, 255);
        if(!CmdLink_SendRequest(CMD_GET_WIFI, Cmd_Data, 200, NULL)) Printf("\nFailed to send request");
        Printf("\nResp=%d", Cmd_Data[0]);
        Printf("\nSSID=%s",Cmd_Data);
        len = strlen(Cmd_Data) + 1;
        Printf("\nPass=%s", Cmd_Data+len);
        break;
      case '7':
        memset(Cmd_Data, 0, 255);
        if(!CmdLink_SendRequest(CMD_GET_ROUTER, Cmd_Data, 200, NULL)) Printf("\nFailed to send request");
        Printf("\nResp=%d", Cmd_Data[0]);
        Printf("\nRounterSSID=%s",Cmd_Data);
        len = strlen(Cmd_Data) + 1;
        Printf("\nRouterPass=%s", Cmd_Data+len);
        break;
      case '8':
        strcpy(Cmd_Data, "new_Rounter");
        len = strlen("new_Rounter") + 1;
        strcpy(Cmd_Data+len , "rounter113");
        len += strlen("rounter113") + 1;
        if(!CmdLink_SendRequest(CMD_SET_ROUTER, Cmd_Data, len, NULL)) Printf("\nFailed to send request");
        Printf("\nResp=%d", Cmd_Data[0]);
        break;
      case '9':
        memset(Cmd_Data, 0, 255);
        if(!CmdLink_SendRequest(CMD_GET_COLOR, Cmd_Data, 200, NULL)) Printf("\nFailed to send request");
        Printf("\nColor=%d",Cmd_Data[0]);
        break;
      case 'a':
        Cmd_Data[0] = 100;
        if(!CmdLink_SendRequest(CMD_SET_COLOR, Cmd_Data, 1, NULL)) Printf("\nFailed to send request");
        Printf("\nResp=%d", Cmd_Data[0]);
        break;   
      case 'b':
        strcpy(Cmd_Data,"newDoorbell");
        len = strlen("newDoorbell") + 1;
        if(!CmdLink_SendRequest(CMD_SET_DEV_NAME, Cmd_Data, len, NULL)) Printf("\nFailed to send request");
        Printf("\nResp=%d", Cmd_Data[0]);
        break;   
      case 'c':
        memset(Cmd_Data, 0, 255);
        if(!CmdLink_SendRequest(CMD_GET_DEV_INFO, Cmd_Data, 200, NULL)) Printf("\nFailed to send request");
        Printf("\nResp=%d", Cmd_Data[0]);
        unsigned long *pmac = (unsigned long *)Cmd_Data;
        Printf("\nMAC=0x%x", *pmac);
        Printf("\nDev name=%s", Cmd_Data+6);
        len = strlen(Cmd_Data+6) + 1;
        Printf("\nProductModel=%s", Cmd_Data+ 6 + len);
        Printf("\nBoardModel=%s", Cmd_Data + 6 +len+8);
        Printf("\nFwModel=%s",Cmd_Data+ 6+len+16);
        Printf("\nFactoryInfo=%s",Cmd_Data+6+len+24);
        break;
      case 'd':
        memset(Cmd_Data, 0, 255);
        if(!CmdLink_SendRequest(CMD_FACTORY_RESET, Cmd_Data, 200, NULL)) Printf("\nFailed to send request");
        Printf("\nResp=%d", Cmd_Data[0]);
        break;
      case 'e':
        memset(Cmd_Data, 0, 255);
        if(!CmdLink_SendRequest(CMD_PING, Cmd_Data, 200, NULL)) Printf("\nFailed to send request");
        Printf("\nResp=%d", Cmd_Data[0]);
        break;  
      case 'f':
        memset(Cmd_Data, 0, 255);
        if(!CmdLink_SendRequest(CMD_RESET, Cmd_Data, 200, NULL)) Printf("\nFailed to send request");
        Printf("\nResp=%d", Cmd_Data[0]);
        break;    
      case 'g':
        memset(Cmd_Data, 0, 255);
        if(!CmdLink_SendRequest(CMD_GET_REPORT_CONTACT, Cmd_Data, 200, NULL)) Printf("\nFailed to send request");
        Printf("\nResp=%d", Cmd_Data[0]);
        Printf("\nEmail=%s",Cmd_Data);
        len = strlen(Cmd_Data) + 1;
        Printf("\nPhone=%s", Cmd_Data+len);
        break;  
      case 'h':
        strcpy(Cmd_Data, "huykhacnguyen1983@gmail.com");
        len = strlen("huykhacnguyen1983@gmail.com") + 1;
        strcpy(Cmd_Data+len , "01269728277");
        len += strlen("01269728277") + 1;
        if(!CmdLink_SendRequest(CMD_REPORT_CONTACT, Cmd_Data, len, NULL)) Printf("\nFailed to send request");
        Printf("\nResp=%d", Cmd_Data[0]);
        break;  
      case 'l':
        Ft_Esd_GetSnapshot("snapshot.jpg");
        break;
#endif
      default: break;
    }
  }
  Printf("\nExit App\n");
  return 0;
}
