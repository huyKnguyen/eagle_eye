#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <poll.h>
#include "main.h"
#include "cmd_link.h"


//###########################################################################################################
//      CONSTANT DEFINITION
//###########################################################################################################
#define MAX_CMD_QUEUE         10
#define MAX_CMD_BUF           250
#define MAX_RESP_BUF          1500
#define RECV_TIMEOUT_SECONDS  2
#define REQUEST_RETRY_COUNT   3

#define MAX_CMD_SOCKETS       1
//###########################################################################################################
//      TYPEDEF DEFINITION
//###########################################################################################################
typedef struct 
{
  uint8_t cmd;
  uint8_t *data;
  uint8_t *resp;
  uint32_t data_len;
  CMD_CALLBACK  callback;
}cmd_queue_t;

typedef struct
{
  int socket;
  int port;
  int remote;
  uint8_t is_used;
  uint8_t is_online;
  uint8_t is_closed;
}cmd_tcp_t;

typedef void* (*CONNECT_THREAD)(void);






//###########################################################################################################
//      Module Level Variables
//###########################################################################################################
static const char MODULE[] = "LINK";

cmd_queue_t Cmd_Queue[MAX_CMD_QUEUE];
static int NotifyUdpSocket;
static int RemoteCmdSocket;
bool CmdLink_IsRunning;
pthread_t CmdLink_Thread_Handle;
pthread_t CmdLink_NotifyThread_Handle;
uint8_t CmdLink_CmdBuf[MAX_CMD_BUF];
uint8_t CmdLink_CmdResp[MAX_RESP_BUF];
int CmdLink_RespLen;
static cmd_tcp_t CmdSocket;
int HubNotifyUdpSocket;  


pthread_t ConnectThreadHandle; 
uint8_t ConnectThreadRunning;  //whether or not the thread to connect TCP CMD running 
uint8_t ForceStopConnect;       //whether or not it needs to stop the thread to connect TCP CMD

//###########################################################################################################
//      PRIVATE FUNCTION DECLARATION
//###########################################################################################################
static bool cmdLink_Send(uint8_t cmd, uint8_t *data, uint32_t len);
static void cmdLink_Thread(void *arg);
static void* cmdLink_NotifyThread(void *arg);
static bool cmdLink_CreateTCPSocket(int *socket,uint32_t port);
static int cmdLink_ConnectTcpSocket(int socket);
static void cmdLink_StartConnectSockets();
static void* cmdLink_ConnectCmdSocket(void *arg);


//###########################################################################################################
//      PUBLIC FUNCTION DECLARATION
//###########################################################################################################
void sigpipe_handler()
{
    printf("\nDisconnected");
    //close(CmdSocket[CmdLink_ActiveSocketIndex].remote); 
    //CmdSocket[CmdLink_ActiveSocketIndex].is_online = 0;
    //CmdLink_StartConnectSockets();
}


void CmdLink_Init()
{
  struct sockaddr_in serv_addr; 
  struct sockaddr_in udp_port;
  int err;

  Printf("[INFO][%s]CmdLink Init\n",MODULE);
  
  CmdLink_IsRunning = 0;
  
  // instal sigpipe handler
  signal(SIGPIPE,sigpipe_handler);
    
  /* Create UDP socket to send greeting to device */
  HubNotifyUdpSocket = socket(PF_INET, SOCK_DGRAM, 0); // create a UDP socket
  if(HubNotifyUdpSocket<=0) 
  { 
    Printf("[ERROR][%s]Greeting UDP socket failed\n",MODULE); 
    return;
  }  
  /* Bind this UDP socket so that we can receive response from device */
  udp_port.sin_family = AF_INET;
  udp_port.sin_port = htons(UDP_GREETING_PORT); 
  udp_port.sin_addr.s_addr = inet_addr("0.0.0.0");
  memset(udp_port.sin_zero, '\0', sizeof udp_port.sin_zero);  
  bind(HubNotifyUdpSocket, (struct sockaddr *) &udp_port, sizeof(udp_port));

  /* Create TCP socket to send command to camera */
  CmdSocket.is_closed = 1;
  cmdLink_StartConnectSockets();

  /* Reset the cmd queue */
  for(int i=0; i<MAX_CMD_QUEUE; i++)
  {
    Cmd_Queue[i].cmd = 0;
  }

  /* Start thread to receive notification from Doorbell */
  err = pthread_create(&CmdLink_NotifyThread_Handle, NULL, &cmdLink_NotifyThread, NULL);
  if(err != 0)
  {
    Printf("[ERROR][%s]Cannot start Notify thread [%s]\n",MODULE, strerror(err));
    return false;
  }

}

void CmdLink_FreeCmdSocket()
{
    //NOTE: Need to close both remote and TCP socket so that device cannot reconnect anymore
    CmdSocket.is_online = 0;
    CmdSocket.is_used = 0;
    CmdSocket.is_closed = 1;
    ForceStopConnect = 1;  
    close(CmdSocket.socket);
    close(CmdSocket.remote);
}

void CmdLink_ResetCmdSocket()
{
  CmdSocket.is_online = 0;
  close(CmdSocket.remote);
}

request_status_t CmdLink_SendCmd(uint8_t cmd)
{
  uint8_t resp;
  bool ret;

  if(CmdSocket.is_online == 0) return REQUEST_FAILED;
  
  ret = CmdLink_SendRequest(cmd, &resp, 1,(CMD_CALLBACK)NULL);
  if(ret) return resp;
  else  return REQUEST_FAILED; 
}

bool CmdLink_SendPing()
{
  uint8_t retries;
  bool ret = false;

  retries = REQUEST_RETRY_COUNT;
  CmdLink_CmdBuf[0] = 1;
  CmdLink_CmdBuf[1] = CMD_PING;
  while(retries)
  {
    write(CmdSocket.remote, CmdLink_CmdBuf, 2);
    CmdLink_RespLen = recv(CmdSocket.remote, CmdLink_CmdResp, MAX_CMD_BUF, 0);
    if(CmdLink_RespLen > 0) break;
    Printf("[WARN][%s]Retry Ping...\n", MODULE);
    retries--;
  }
  if(CmdLink_RespLen > 0)
  {
    ret = true;
  }
  else
  {
      //Auto reconnect socket if it fails to send request
    close(CmdSocket.remote); 
    CmdSocket.is_online = 0;
    cmdLink_StartConnectSockets();
  }
  return ret;
}

//NOTE: It will overwrite *data with response from device
bool CmdLink_SendRequest(uint8_t cmd, uint8_t *data, uint32_t len, CMD_CALLBACK callback)
{
  int i;
  bool ret = false;
  uint8_t *ptr = NULL;
  
  if(len > MAX_REQUEST_DATA) return false;
  
  if(callback == NULL)  //it is a blocking request
  {
    //Send request to Doorbell and store response into CmdLink_CmdResp[] buffer
    ret = cmdLink_Send(cmd, data, len);
    if(ret)
    {
      //Copy response from Doorbell into requester's buffer
      if(len > CmdLink_RespLen) len = CmdLink_RespLen;
      memcpy(data, CmdLink_CmdResp,len);
    }
    else
    {
      //Auto reconnect socket if it fails to send request
      close(CmdSocket.remote); 
      CmdSocket.is_online = 0;
      cmdLink_StartConnectSockets();
    }
  }
  else    //It is an unblocking request
  {
    //Look for a free slot in the queue
    for(i=0; i<MAX_CMD_QUEUE; i++)
    {
      if(Cmd_Queue[i].cmd == 0) break;
    }
    //Return false if no free slot found
    if(i == MAX_CMD_QUEUE) 
    {
      Printf("[WARN][%s]Cmd Queue is full\n", MODULE);
      return false;
    }
    //Insert the new command to the queue
    Cmd_Queue[i].cmd = cmd;
    if(cmd & SET_REQ)
    {
      ptr = (uint8_t *)malloc(len);
      if(ptr == NULL)
      {
        Printf("[ERROR][%s]Failed to allocated\n",MODULE);
        return false;
      }
      memcpy(ptr, data, len);
      Cmd_Queue[i].data = ptr;
      Cmd_Queue[i].data_len = len;
    }
    else
    {
      Cmd_Queue[i].data = NULL;
      Cmd_Queue[i].data_len = 0;
    }
    Cmd_Queue[i].callback = callback;
    //Start Cmd Thread to send the request to Doorbell
    if(!CmdLink_IsRunning)
    {
      int err = pthread_create(&CmdLink_Thread_Handle, NULL, &cmdLink_Thread, NULL);
      if(err != 0)
      {
        Printf("[ERROR][%s]Cannot start Cmd thread [%s]\n",MODULE, strerror(err));
        return false;
      }
    }
    ret = true;
  }

  return ret;
}

/*
  For hub to send greeting and assigned TCP port number to device in router network
*/
bool CmdLink_SendNotification(char *ip, uint8_t *data, uint8_t len)
{
  struct sockaddr_in recipient;
  struct pollfd fd;
  uint8_t response, temp;
  int ret;

  //prepare to check receive timeout
  fd.fd = HubNotifyUdpSocket;
  fd.events = POLLIN;
  //specify device destination  
  recipient.sin_family = AF_INET;
  recipient.sin_port = htons(UDP_GREETING_PORT);
  recipient.sin_addr.s_addr = inet_addr(ip);

  //First to flush old data in Rx buffer
  ret = 1;
  while(ret > 0)
  {
    ret = poll(&fd,1,300);
    if(ret > 0)
    {
      recv(HubNotifyUdpSocket,&temp,1,0);
    }
  }
  int retries = 3;
  while(retries)
  {
    //Send 10 messages for each try to avoid UDP missing packets
    for(int i=0; i<10; i++)
    {
      sendto(HubNotifyUdpSocket, data, len, 0, (struct sockaddr*) &recipient, sizeof(recipient));
      usleep(100);
    }
    ret = poll(&fd, 1, 500);   //wait 200ms
    if(ret > 0)
    {
      recv(HubNotifyUdpSocket,&response,1,0);
      if(response == CMD_ACK)
      {
        break;
      }
    }
    sleep(1);
    Printf("[WANR][%s]Retry...\n",MODULE);  
    retries--;
  }

  if(retries > 0)
  {
    Printf("[INFO][%s]Receive response from device\n",MODULE);
    return true;
  }
  else
  {
    Printf("[WARN][%s]No response from device\n",MODULE);
    return false;
  }

}


//###########################################################################################################
//      PRIVATE FUNCTION DECLARATION
//###########################################################################################################
static void cmdLink_StartConnectSockets()
{ 
  int err;

  //If this socket was closed, need to re-create here
  if(CmdSocket.is_closed == 1)
  {
    cmdLink_CreateTCPSocket(&CmdSocket.socket, CMD_TCP_PORT);
    CmdSocket.is_closed = 0;
  }
  err = pthread_create(&ConnectThreadHandle, NULL, cmdLink_ConnectCmdSocket, NULL);
  if(err != 0)
  {
    Printf("[ERROR][%s]Cannot start Connect thread [%s]\n",MODULE, strerror(err));
    return false;
  }
}

static void* cmdLink_ConnectCmdSocket(void *arg)
{
  Printf("[INFO][%s]Start connect thread\n",MODULE);
  ConnectThreadRunning = 1;
  while(1)
  {
    CmdSocket.remote = cmdLink_ConnectTcpSocket(CmdSocket.socket);
    if(ForceStopConnect || CmdSocket.remote >= 0) break;
  }
  if(CmdSocket.remote >= 0)
  {
    CmdSocket.is_online = 1;
    Printf("[INFO][%s]CMD Socket online\n",MODULE);
  }
  else
  {
    CmdSocket.is_online = 0;
    Printf("[INFO][%s]CMD Socket offline\n",MODULE);
  }
  ForceStopConnect = 0;
  ConnectThreadRunning = 0;
}

static bool cmdLink_CreateTCPSocket(int *tcp_socket,uint32_t port)
{
  int new_socket;
  struct sockaddr_in serv_addr;
  /* Create TCP socket for CMD link */ 
  new_socket = socket(AF_INET, SOCK_STREAM, 0);
  if(new_socket < 0)
  {
    Printf("[ERROR][%s]Failed to create TCP socket\n",MODULE);
    return false;
  }
  //Set attribute SO_REUSEADDR so that we can bind it again next time
  int reuse = 1;
  setsockopt(new_socket, SOL_SOCKET, SO_REUSEADDR, (const char*)&reuse, sizeof(reuse));
  
  memset(&serv_addr, '0', sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  serv_addr.sin_port = htons(port); 
   
  if(bind(new_socket, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
  {
    Printf("[ERROR][%s]Failed to bind CMD socket\n", MODULE);
    return false;
  }

  *tcp_socket = new_socket;
  return true;
}

static int cmdLink_ConnectTcpSocket(int socket)
{
  int len = sizeof(struct sockaddr_in);
  int remote_socket;
  struct sockaddr_in client;
  struct timeval tv;
  
  tv.tv_sec = RECV_TIMEOUT_SECONDS;
  tv.tv_usec = 0;
  Printf("[INFO][%s]Waiting connect CMD socket %d\n", MODULE,socket);

  listen(socket, 3); 
  remote_socket = accept(socket, (struct sockaddr *)NULL, NULL);
  if(remote_socket < 0)
  {
    Printf("[ERROR][%s]Failed to accept CMD socket\n", MODULE);
    pthread_exit(NULL);
  }
  Printf("[INFO][%s]Connected\n", MODULE);
  setsockopt(remote_socket, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);    //Set Rx Timeout
  return remote_socket;

}

static void* cmdLink_NotifyThread(void *arg)
{

  struct sockaddr_in serv_addr; 
  uint8_t rx_len;
  uint8_t notify_buf[MAX_EVT_DATA];
  struct sockaddr_in si_other;
  int slen = sizeof(si_other);
  
  /* Create UDP socket for Notification link */ 
  NotifyUdpSocket = socket(PF_INET, SOCK_DGRAM, 0); // create a UDP socket
  if(NotifyUdpSocket<=0) 
  { 
    Printf("[ERROR][%s]Failed to create Notify socket\n", MODULE);
    return 0; 
  }
  struct sockaddr_in udp_port;
  udp_port.sin_family = AF_INET;
  udp_port.sin_port = htons(RX_NOTIFY_UDP_PORT); 
  udp_port.sin_addr.s_addr = inet_addr("0.0.0.0");
  memset(udp_port.sin_zero, '\0', sizeof udp_port.sin_zero);  
  bind(NotifyUdpSocket, (struct sockaddr *) &udp_port, sizeof(udp_port)); // bind the socket

  //recipient.sin_family = AF_INET;
  //recipient.sin_port = htons(RX_NOTIFY_UDP_PORT);
  //recipient.sin_addr.s_addr = inet_addr(DOORBELL_IP);
  Printf("Start to listen notification\n");
  while(1)
  {
    //receive data from client
    rx_len = recvfrom(NotifyUdpSocket, notify_buf, MAX_EVT_DATA, 0, (struct sockaddr *)&si_other, &slen);
    if(rx_len > 0)
    {
      Printf("[INFO][%s]Receive event %d[%d]\n",MODULE, notify_buf[0],rx_len);
      //TODO: Handle notification from camera
      //For now, just send ACK response
      if(1) //(SysInfo_EvtHandler(notify_buf, rx_len))
      {
        //Send response
        notify_buf[0] = NOTIFY_ACK;
        sendto(NotifyUdpSocket, notify_buf, 1, 0, (struct sockaddr*) &si_other, sizeof(si_other));
      }
    }
  }

}

static void cmdLink_Thread(void *arg)
{
  bool cmd_executed;
  bool ret;
  struct timeval tv;
 
  
  CmdLink_IsRunning = true;

  /* Check command in the cmd queue */
  while(true)
  {
    cmd_executed = 0;
    for(int i=0; i<MAX_CMD_QUEUE; i++)
    {
      if(Cmd_Queue[i].cmd)
      {
        //Send request to Doorbell
        ret = cmdLink_Send(Cmd_Queue[i].cmd, Cmd_Queue[i].data, Cmd_Queue[i].data_len);
        //Involve callback
        if(Cmd_Queue[i].callback != NULL)
        {
          (Cmd_Queue[i].callback)((request_status_t)ret, CmdLink_CmdResp, CmdLink_RespLen);
        }

        //Auto reconnect socket if it fails to send request
        if(ret == false)
        {
          close(CmdSocket.remote); 
          CmdSocket.is_online = 0;
          cmdLink_StartConnectSockets();
        }
        
        //Free resource of this command slot
        if(Cmd_Queue[i].data != NULL)
        {
          free(Cmd_Queue[i].data);
        }
        Cmd_Queue[i].cmd = 0;   //invalidate the slot 
        cmd_executed = 1;       //set this flag so that it will recheck the CMD queue from beginning
      }
    }
    if(!cmd_executed) break;    //break while() loop if the queue is empty
  }

  CmdLink_IsRunning = false;
}

static bool cmdLink_Send(uint8_t cmd, uint8_t *data, uint32_t len)
{
  int retry_count;
  bool ret;
  int tx_len;
  
  if(cmd & SET_REQ)
  {
    CmdLink_CmdBuf[0] = len + 1;  //[0] = cmd + data len
    CmdLink_CmdBuf[1] = cmd;
    memcpy(&CmdLink_CmdBuf[2], data, len);
    tx_len = len + 2;
  }
  else
  {
    CmdLink_CmdBuf[0] = 1;
    CmdLink_CmdBuf[1] = cmd;
    tx_len = 2;
  }

  retry_count = REQUEST_RETRY_COUNT;
  while(retry_count--)
  {    
    //Send request to device
    int remote = CmdSocket.remote;
    write(remote, CmdLink_CmdBuf, tx_len);
    //Readback response
    //CmdLink_RespLen = read(RemoteCmdSocket, CmdLink_CmdResp, MAX_CMD_BUF); //it will timoeut after RECV_TIMEOUT_SECONDS
    CmdLink_RespLen = recv(remote, CmdLink_CmdResp, MAX_CMD_BUF, 0);
    if(CmdLink_RespLen > 0) break;
    Printf("[WARN][%s]Retry...\n", MODULE);
  }

  if(retry_count > 0) ret =  true;    //if it can receive response from Doorbell 
  else 
  {
    Printf("[WARN][%s]No response from Doorbell\n",MODULE);
    ret =  false;
  }
  return ret;
}
