
#include "utils.h"

const char CharSet[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJK...";


void Utils_GetInterfaceMAC(uint8_t *mac)
{
 struct ifreq ifr;
 int fd = socket(AF_INET, SOCK_DGRAM, 0);
 ifr.ifr_addr.sa_family = AF_INET;      //IPv4 IP address
 strncpy(ifr.ifr_name, "wlan0", IFNAMSIZ-1);  //IP address of wlan0
 ioctl(fd, SIOCGIFHWADDR, &ifr);
 close(fd);
 for(int i=0; i<6; i++)
 {
    mac[i] = ifr.ifr_hwaddr.sa_data[i];
 }
}

void Utils_GetInterfaceIP(char* ip)
{
  struct ifreq ifr;
  int fd = socket(AF_INET, SOCK_DGRAM, 0);
  ifr.ifr_addr.sa_family = AF_INET;      //IPv4 IP address
  strncpy(ifr.ifr_name, "wlan0", IFNAMSIZ-1);  //IP address of wlan0
  ioctl(fd, SIOCGIFADDR, &ifr);
  close(fd);
  strcpy(ip, inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
}

unsigned long Utils_GetInterfaceHexIP()
{
  struct ifreq ifr;
  int fd = socket(AF_INET, SOCK_DGRAM, 0);
  ifr.ifr_addr.sa_family = AF_INET;      //IPv4 IP address
  strncpy(ifr.ifr_name, "wlan0", IFNAMSIZ-1);  //IP address of wlan0
  ioctl(fd, SIOCGIFADDR, &ifr);
  close(fd);
  return ((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr.s_addr;

}

void Utils_GetBroadcastIP(char *ip)
{
  struct ifreq ifr;
  int fd = socket(AF_INET, SOCK_DGRAM, 0);
  ifr.ifr_addr.sa_family = AF_INET;      //IPv4 IP address
  strncpy(ifr.ifr_name, "wlan0", IFNAMSIZ-1);  //IP address of wlan0
  ioctl(fd, SIOCGIFBRDADDR, &ifr);
  close(fd);
  strcpy(ip, inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
}

void Utils_RandString(char *str, uint8_t size)
{ 
  if (size) {
      --size;
      for (size_t n = 0; n < size; n++) {
          int key = rand() % (int) (sizeof CharSet - 1);
          str[n] = CharSet[key];
      }
      str[size] = '\0';
  }
}


