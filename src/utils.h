#ifndef UTILS_H
#define UTILS_H

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <net/if.h>
#include <net/if_arp.h>
#include <arpa/inet.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <unistd.h>

void Utils_GetInterfaceMAC(uint8_t *mac);
void Utils_GetInterfaceIP(char* ip);
void Utils_GetBroadcastIP(char *ip);
unsigned long Utils_GetInterfaceHexIP();
void Utils_RandString(char *str, uint8_t size);



#endif  //UTILS_H
